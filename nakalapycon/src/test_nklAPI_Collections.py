# -*- coding: utf-8 -*-
"""
Created on Fri Aug 27 09:17:32 2021

@author: Michael Nauge (Université de Poitiers)

"""


import NklTarget as nklT
import nklAPI_Collections as nklC
import nklAPI_Datas as nklD



def get_collections_test():
    
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")
    
     
    print("Test d'un collectionIdentifier qui n'existe pas >> ")  
    dataIdentifier = "10.34847/nkl.ffabdg37"
    r = nklC.get_collections(nklT_prodEmpty, dataIdentifier)
    print(r)   
    
    print("Test d'un collectionIdentifier qui existe >>")
    dataIdentifier = "10.34847/nkl.ffabdg38"
    r = nklC.get_collections(nklT_prodEmpty, dataIdentifier)
    print(r)
    
        
    print("Test d'un dataIdentifier qui existe avec précision qdc >>")
    dataIdentifier = "10.34847/nkl.ffabdg38"
    r = nklC.get_collections(nklT_prodEmpty, dataIdentifier, metadataFormat="qdc")
    print(r)
    
    
    
def put_collections_test():
    # pour pouvoir modifier une data on va d'abord en creer une
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_testUserunakala2 = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    # identifier à obtenir dans la réponse de création
    rd = post_collections_test()
    
    if rd.isSuccess:
        
        identifier = rd.dictVals['payload']['id']
        print("le doi de la data a modifier est", identifier)
        
        # mise à jour de quelques metas
        dicoMetas = {}
        dicoMetas["metas"] = []
        # création de la méta nakala_title
        # c'est la seule méta obligatoire
        metaTitle={"value": "DBG - Collection by nakalapycon (updated by PUT nakalapycon)",
          "lang": "fr",
          "typeUri": "http://www.w3.org/2001/XMLSchema#string",
          "propertyUri": "http://nakala.fr/terms#title"
          }
        dicoMetas["metas"].append(metaTitle)

    
        r = nklC.put_collections(nklT_testUserunakala2, identifier, dicoMetas)
        print(r)
    
def delete_collections_test():
    # pour pouvoir tester la suppression d'une collection il faut en creer une avant.
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_testUserunakala2 = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    # identifier à obtenir dans la réponse de création
    rd = post_collections_test()
    
    if rd.isSuccess:
        
        identifier = rd.dictVals['payload']['id']
        print("le doi de la data a supprimmer est", identifier)
    
        r = nklC.delete_collections(nklT_testUserunakala2, identifier)
        print(r)
    
    
def post_collections_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_testUserunakala2 = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    
    # creation d'un dictionnaire collection
    dicoMetas = {}
    
    # choisir la valeur du status de la collection
    # soit public soit private
    dicoMetas["status"] = "private"
    
    dicoMetas["metas"] = []
    # création de la méta nakala_title
    # c'est la seule méta obligatoire
    metaTitle={"value": "DBG - Collection by nakalapycon",
      "lang": "fr",
      "typeUri": "http://www.w3.org/2001/XMLSchema#string",
      "propertyUri": "http://nakala.fr/terms#title"
      }
    dicoMetas["metas"].append(metaTitle)
    

    rd = nklC.post_collections(nklT_testUserunakala2, dicoMetas)
    print(rd)
    
    if rd.isSuccess:
        print("le doi de la collection est", rd.dictVals['payload']['id'])
            
    return rd
    

def get_collections_datas_test():
        
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")
    
     
    print("Test d'un collectionIdentifier qui n'existe pas >> ")  
    dataIdentifier = "10.34847/nkl.ffabdg37"
    r = nklC.get_collections_datas(nklT_prodEmpty, dataIdentifier)
    print(r)   
    

    
        
    print("Test d'un dataIdentifier qui existe  >>")
    dataIdentifier = "10.34847/nkl.ffabdg38"
    r = nklC.get_collections_datas(nklT_prodEmpty, dataIdentifier)
    print(r)   
    
    nbDataInThisPage = len(r.dictVals['data'])
    print("nbDataInThisPage",nbDataInThisPage)
    
    # afficher le nombre de page de resultat disponible:
    if r.isSuccess:
        lastPage = r.dictVals['lastPage']
        print("nb page dispo: ", lastPage)
        
        #on appel la dernière page
        r_last = nklC.get_collections_datas(nklT_prodEmpty, dataIdentifier,page=lastPage)
        print(r_last)  
        
        nbDataInThisPage = len(r_last.dictVals['data'])
        print("nbDataInThisPage",nbDataInThisPage)
    

def post_collections_datas_test():
    # on va dabord creer une data puis on va crééer une collection et enfin on va ajouter la data dans la collection
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_testUserunakala2 = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    # creation d'un dictionnaire data
    dicoMetas = {}
    #dicoMetas["collectionsIds"] = ["10.34847/nkl.b9eblul0"]
    dicoMetas["collectionsIds"] = []


    dicoMetas["status"] = "pending"
    #dicoMetas["status"] = "published"

    dicoMetas["metas"] = []
    
    metaType = {"value": "http://purl.org/coar/resource_type/c_c513",
                "lang": "fr",
                "typeUri": "http://www.w3.org/2001/XMLSchema#anyURI",
                "propertyUri": "http://nakala.fr/terms#type"
                }
    dicoMetas["metas"].append(metaType)
    
    
    metaTitle={"value": "DBG - data image (nakalapycon)",
      "lang": "fr",
      "typeUri": "http://www.w3.org/2001/XMLSchema#string",
      "propertyUri": "http://nakala.fr/terms#title"
      }
    dicoMetas["metas"].append(metaTitle)

    metaAuthor = {"value": {
        "givenname": "Mic",
        "surname": "Mac",
        "orcid":"0000-0000-4242-4242"
         },
      "propertyUri": "http://nakala.fr/terms#creator"
      }
    dicoMetas["metas"].append(metaAuthor)
    
    metaCreated={"value": "1961-01-01",
      "typeUri": "http://www.w3.org/2001/XMLSchema#string",
      "propertyUri": "http://nakala.fr/terms#created"
    }
    dicoMetas["metas"].append(metaCreated)
    
    metaLicense={"value": "CC-BY-4.0",
                 "typeUri": "http://www.w3.org/2001/XMLSchema#string",
                 "propertyUri": "http://nakala.fr/terms#license"
                 }
    dicoMetas["metas"].append(metaLicense)
    
    
    metaSubjectFr={"value": "sujetEnFre",
                 "lang": "fr",
                 "typeUri": "http://www.w3.org/2001/XMLSchema#string",
                 "propertyUri": "http://purl.org/dc/terms/subject"
                 }
    dicoMetas["metas"].append(metaSubjectFr)
    
    metaSubjectEn={"value": "sujetEnEng",
                 "lang": "en",
                 "typeUri": "http://www.w3.org/2001/XMLSchema#string",
                 "propertyUri": "http://purl.org/dc/terms/subject"
                 }
    dicoMetas["metas"].append(metaSubjectEn)
    
    # ajout d'un group utilisateur avec les droits admin
    """
    dicoMetas["rights"]=[]
    dicoMetas["rights"].append({'id':"cb5f5980-056e-11ec-9b31-52540084ccd3",'role':"ROLE_ADMIN"})
    """
    
    # ajout d'un role admin à unakala3 8badb61c-ad90-11eb-8529-0242ac130003
    dicoMetas["rights"]=[]
    dicoMetas["rights"].append({'id':"8badb61c-ad90-11eb-8529-0242ac130003",'role':"ROLE_ADMIN"})
    
    # envoi d'un fichier pour obtenir son sha1
    pathFile = "./../test/files/test_0001-0001.jpg"
    r = nklD.post_datas_uploads(nklT_testUserunakala2, pathFile)
    
    print(r)
    
    if r.isSuccess:
    
        print("le sha1 est", r.dictVals['sha1'])
        
        dicoMetas["files"] = [{'sha1':r.dictVals['sha1'],'description':"ma belle description de fichier"}]
        
        # puisque l'envoi du fichier c'est bien passé,
        # on envoi la data 
        rd = nklD.post_datas(nklT_testUserunakala2, dicoMetas)
        print(rd)
        
        if rd.isSuccess:
            identifierData = rd.dictVals['payload']['id']
            print("le doi de la data est", identifierData)
            
            # on va creer une collection
            # identifier à obtenir dans la réponse de création
            rc = post_collections_test()
            
            if rc.isSuccess:
                
                identifierCollection = rc.dictVals['payload']['id']
                
                #on va ajouter la nouvelle data dans cette nouvelle collection
                listData = [identifierData]
                rp = nklC.post_collections_datas(nklT_testUserunakala2, identifierCollection, listData)
                print(rp)
    
#get_collections_test()
#post_collections_test()
#put_collections_test()
#delete_collections_test()
#get_collections_datas_test()
#post_collections_datas_test()