# -*- coding: utf-8 -*-
"""
Created on Wed May 18 16:24:44 2022

@author: Michael Nauge (Université de Poitiers)

"""


import NklTarget as nklT
import nklAPI_Search as nklS


def get_search_datas_test():
    
    # chercher une data par str_dcterms_identifier:UPOI_ATP_0001_0002
    
    
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")
    
    #rs = nklS.get_search_datas(nklT_prodEmpty, q="str_dcterms_identifier:UPOI_ATP_0001_0002", fq="", facet="", order="relevance")
    #rs = nklS.get_search_datas(nklT_prodEmpty, q="str_dcterms_identifier:UPOI_ATP_0001_0003", fq="", facet="", order="relevance")
    #rs = nklS.get_search_datas(nklT_prodEmpty, q="UPOI_ATP_0001", fq="scope=data", facet="", order="relevance")
    #rs = nklS.get_search_datas(nklT_prodEmpty, q="UPOI_ATP_0001", fq="scope=data", facet="", order="relevance")
    #rs = nklS.get_search_datas(nklT_prodEmpty, q="UPOI_ATP_", fq="", facet="", order="relevance")
    #rs = nklS.get_search_datas(nklT_prodEmpty, q="str_dcterms_identifier:UPOI_ATP_0001", fq="", facet="", order="relevance")
    
    # Exemple de Search très précis où on précise 
    #   une cote dans un dcterms_identifier
    #   uniquement des datas (pas de collection)
    #   l'appartenance à une collection mère (collection francoralité: )
    rs = nklS.get_search_datas(nklT_prodEmpty, q="str_dcterms_identifier:UPOI_ATP_0001_0002", fq="scope=data;collection=11280/e331def8", facet="", order="relevance")
    # attention à ne pas être trop précis quand même pour ne pas rater quelque chose d'existant mais simplement mal poussé
    

    #print(rs)
    
    if rs.isSuccess:
        print("total results", rs.dictVals['totalResults'])
        
        for data in rs.dictVals["datas"]:
            print("----------------")
            print("nakala identifier",data['identifier'])
            
            # si ça remonte des datas on va avoir des files
            if "files" in data:
                print("files")
                
                for file in data["files"]:
                    print(file["name"], file["sha1"])
                    
            # si ça remonte des collections on va avoir des datasIds
            if "datasIds" in data:
                print("datasIds")
                
                for dataids in data["datasIds"]:
                    print(dataids)
                
            print("-----------------")
    else:
        print(rs)
        
        
def get_search_authors_test():
    
    # chercher une data par str_dcterms_identifier:UPOI_ATP_0001_0002
    
    
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")
    

    rs = nklS.get_search_authors(nklT_prodEmpty, q="Michel")


    #print(rs)
    
    if rs.isSuccess:
        print(rs)
        print("total results", len(rs.dictVals))
        
        for author in rs.dictVals:
            print("----- author ------")
           
            print(author)
           
                
            print("-----------------")
    else:
        print(rs)


#get_search_datas_test()
get_search_authors_test()