# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 16:39:24 2021

@author: Michael Nauge (Université de Poitiers)
"""


import NklTarget as nklT
import nklUtils as nklUt
import nklAPI_Search as nklS
 


def put_collections_datas_rights_test():
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    collectionId = "10.34847/nkl.b0dd3qxb"
    userGroupId = "cb5f5980-056e-11ec-9b31-52540084ccd3"
    role = "ROLE_ADMIN"
    
    listR = nklUt.put_collections_datas_rights(nklT_test, collectionId, userGroupId, role)
    
    for rep in listR:
        print(rep)


 



def isFileNameInData_test():
    checkedFilename = "UPOI_ATP_0001_0002_003.mp3"
    #https://nakala.fr/10.34847/nkl.ac5bj2m1
    
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")

    rs = nklS.get_search_datas(nklT_prodEmpty, q="str_dcterms_identifier:UPOI_ATP_0001_0002", fq="scope=data", facet="", order="relevance")
    
    if rs.isSuccess:
        
        nbResults = rs.dictVals['totalResults']
        print("total data results", nbResults)
        # si on a trouvé 1 data correspondante
        # on va parcourir ses dichier pour vérifier si il y a celui
        # qui nous interesse
        if nbResults==1:
            print(rs.dictVals["datas"][0]['identifier'])
            res= nklUt.isFileNameInData(rs, checkedFilename)
            
            print(res)
        
def isFileSha1InData_test():
    checkedSha1 = "866e9cc708b48ea5dc82b870de2606f6b55b1982"
    #https://nakala.fr/10.34847/nkl.ac5bj2m1
    
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")

    rs = nklS.get_search_datas(nklT_prodEmpty, q="str_dcterms_identifier:UPOI_ATP_0001_0002", fq="scope=data", facet="", order="relevance")
    
    if rs.isSuccess:
        
        nbResults = rs.dictVals['totalResults']
        print("total data results", nbResults)
        # si on a trouvé 1 data correspondante
        # on va parcourir ses dichier pour vérifier si il y a celui
        # qui nous interesse
        if nbResults==1:
            print(rs.dictVals["datas"][0]['identifier'])
            
            res = nklUt.isFileSha1InData(rs, checkedSha1)
            print(res)
            
   
def isFileNameInUploads_test():   
    checkedFilename = "1BC34D4B-1164-4892-89A9-B93FF6323BD9.jpeg"
    
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")

    # récupération/vérification des files et leurs sha1 déposé non encore associés à une data
    nklRes = nklUt.get_datas_uploads(nklT_test)
    print(nklRes)
    
    res = nklUt.isFileNameInUploads(nklRes, checkedFilename)
    print(res)
    
   
def isFileSha1InUploads_test():   
    checkedSha1 = "c033f4874e2cce2aac21311fdce137b35fda759c"
    
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")

    # récupération/vérification des files et leurs sha1 déposé non encore associés à une data
    nklRes = nklUt.get_datas_uploads(nklT_test)
    print(nklRes)
    
    res = nklUt.isFileSha1InUploads(nklRes, checkedSha1)
    print(res)
    
    
#put_collections_datas_rights_test()
isFileNameInData_test()  
#isFileSha1InData_test()
#isFileNameInUploads_test()
#isFileSha1InUploads_test()


