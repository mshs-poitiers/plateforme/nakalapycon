# -*- coding: utf-8 -*-
"""
Created on Wed Aug 18 09:37:06 2021

@author: Michael Nauge, Université de Poitiers
"""


import constantes as nklC


def vocabTypeGetUriByKey_test():
    
    uri = nklC.vocabTypeGetUriByKey("text")
    print("return Uri", uri)

    # on lève une assertion si la valeur renvoyé ne correspond pas à l'attendu
    assert  uri == "http://purl.org/coar/resource_type/c_18cf"

    




def vocabTypeGetKeyByUri_test():
    key = nklC.vocabTypeGetKeyByUri("http://purl.org/coar/resource_type/c_18cf")
    print("return Key", key)
    
    # on lève une assertion si la valeur renvoyé ne correspond pas à l'attendu
    assert key=="text"




vocabTypeGetUriByKey_test()

vocabTypeGetKeyByUri_test()