# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 16:39:24 2021

@author: Michael Nauge (Université de Poitiers)
"""


import NklTarget as nklT
import nklAPI_Vocabularies as nklV


def get_vocabularies_licenses_test():
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    r = nklV.get_vocabularies_licenses(nklT_test)
    print(r)
    
    print("----------------")
    if r.isSuccess:
        for licence in r.dictVals:
            print('code:',licence['code'])
            print('name:',licence['name'])
            print('url:',licence['url'])
            
            
            
            
            
def get_vocabularies_datatypes():
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    r = nklV.get_vocabularies_datatypes(nklT_test)
    print(r)
    

def get_vocabularies_properties_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    r = nklV.get_vocabularies_properties(nklT_test)
    print(r)
        
    
    
def get_vocabularies_metadatatypes_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    r = nklV.get_vocabularies_metadatatypes(nklT_test)
    print(r)
            
 
def get_vocabularies_languages_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    """
    print("recherche par defaut")
    r = nklV.get_vocabularies_languages(nklT_test)
    print(r)   
    
    print("recherche avec valeurs")
    r = nklV.get_vocabularies_languages(nklT_test,code="fr")
    print(r)    
    """
    print("recherche avec valeurs")
    r = nklV.get_vocabularies_languages(nklT_test,q="french")
    print(r)    
    
    if r.isSuccess:
        print("------")
        for lang in r.dictVals:
            print(lang)
    
#get_vocabularies_licenses_test() 
#get_vocabularies_datatypes() 
#get_vocabularies_properties_test()
#get_vocabularies_metadatatypes_test()
get_vocabularies_languages_test()


