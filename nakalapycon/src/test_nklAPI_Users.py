# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 16:39:24 2021

@author: Michael Nauge (Université de Poitiers)
"""


import NklTarget as nklT
import nklAPI_Users as nklU


def get_users_me_test():
    
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : f41f5957-d396-3bb9-ce35-a4692773f636
    nklT_test = nklT.NklTarget(isNakalaProd=False, apiKey="f41f5957-d396-3bb9-ce35-a4692773f636")
    
    r = nklU.get_users_me(nklT_test)
    print(r)
    
    print("----------------")
    if r.isSuccess:
        print('username:',r.dictVals['username'])
        print('givenname:',r.dictVals['givenname'])
        print('surname:',r.dictVals['surname'])
        print('mail:',r.dictVals['mail'])
        print('photo:',r.dictVals['photo'])
        print('firstLogin:',r.dictVals['firstLogin'])
        print('lastLogin:',r.dictVals['lastLogin'])
        print('roles:',r.dictVals['roles'])
        print('apiKey:',r.dictVals['apiKey'])
        print('fullname:',r.dictVals['fullname'])

    
get_users_me_test()  
    


