# -*- coding: utf-8 -*-
"""
Created on Thu Jul 22 11:56:22 2021

@author: Michael Nauge, Université de Poitiers


Les codes de communication avec Nakala sont dispersés dans différents
fichiers mais l'idée est que celà reste transparent 
pour l'utilisateur de la librairie
"""

from NklTarget import *
from NklResponse import *

from nklAPI_Collections import *
from nklAPI_Datas import *
from nklAPI_Groups import *
from nklAPI_Users import *
from nklAPI_Vocabularies import *
from nklAPI_Search import *

from nklUtils import *
from nklPullCorpus import *